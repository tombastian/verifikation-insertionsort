public class Insertionsort {

	public static void insertionsort(int[] arr) {
		if (arr == null || arr.length <= 1)
			return;
		
		int i = 1;
		while (i < arr.length) {
			einsortieren(arr, i);
			i++;
		}
	}

	/*@ normal_behavior
	  @ requires arr != null && i >= 0 && i < arr.length;
	  @ ensures (\forall int x; x >= 0 && x < i; arr[x] <= arr[x + 1]);
	  @ 
	  @ also
	  @ 
	  @ exceptional_behavior
	  @ requires arr == null || i < 0 || i >= arr.length;
	  @ signals (NullPointerException) true;
	  @*/
	public static void einsortieren(int[] arr, int i) {
		int j = i, value = arr[i];
		/*@	loop_invariant j <= i && j >= 0;
		  @ decreasing i + j;
		  @*/
		while (j > 0 && arr[j - 1] > value) {
			arr[j] = arr[j - 1];
			j--;
		}
		arr[j] = value;
	}
	
	public static void main(String[] args) {
		int[] intArr = {5, 10, 4, 9, 52};
		insertionsort(intArr);
		for (int i = 0; i < intArr.length; i++) {
			System.out.println(intArr[i] + ", ");
		}
	}
	
}
