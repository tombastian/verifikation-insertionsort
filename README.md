Der SED ist nicht mit der neuesten Version von Eclipse kompatibel (Stand Januar 2022).
Auf Eclipse Oxygen kann der SED aber problemlos installiert werden.
Eclipse Oxygen kann hier heruntergeladen werden: https://www.eclipse.org/downloads/packages/release/oxygen.
Eine Anleitung zur Installation des SED findet sich hier: https://www.key-project.org/download/
